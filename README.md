# Emagz.Mobi

Emagz.mobi is a place where users can read exclusive and premium magazines worldwide. With a great variety of magazines from over 300 content just for users.

<img src="https://gitlab.com/AdamAbd/nft_auction/uploads/b1a04afe96ef40e9dd99ecc8cbe8dc99/Screenshot_1649260783.png" width="30%"></img> <img src="https://gitlab.com/AdamAbd/nft_auction/uploads/ea589eaae7b939540765e7024158bf34/Screenshot_1649261006.png" width="30%"></img> <img src="https://gitlab.com/AdamAbd/nft_auction/uploads/b1a04afe96ef40e9dd99ecc8cbe8dc99/Screenshot_1649260783.png" width="30%"></img> 

## Features

- Search million type of magazine around the worlds in your hand.
- Read online, so you can read magazine faster.
- Cheaper than any compatitor and it's support any type of payments.

## Tech

Emagz.mobi uses a number of open source projects to work properly:

- [Laravel](https://laravel.com/) - From Frontend to Backend, laravel can hadle it.
- [Flutter](https://flutter.dev/) - Awesome cross platform mobile framework.

## Installation

After clone the repo, install the dependencies and run the application.

```sh
cd emagz
flutter pub get
flutter run -d "device name or id"
```

## Plugins

Emagz.mobi is currently extended with the following packages.
Instructions on how to use them in your own application are linked below.

| Packages | Link |
| ------ | ------ |
| flutter_bloc | [https://pub.dev/packages/flutter_bloc](https://pub.dev/packages/flutter_bloc) |
| hydrated_bloc | [https://pub.dev/packages/hydrated_bloc](https://pub.dev/packages/hydrated_bloc) |
| carousel_slider | [https://pub.dev/packages/carousel_slider](https://pub.dev/packages/carousel_slider) |
